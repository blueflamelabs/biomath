@isTest
public class RollupChildOpprotunityHandler1Test {
    
    @isTest
    public static void positiveTestForInsert(){
        
        Account acc = TestDataFactory.acctRec();
        
        insert acc;
        
        List<Opportunity> parentOppList = TestDataFactory.getParentOpportunity(acc.Id);
        
        insert parentOppList;
        
        List<Opportunity> childOppList = TestDataFactory.getChildOpportunity(acc.Id, parentOppList[0].id);
        
        Test.startTest();
        
        insert childOppList;
        
        System.debug('--19--Testclass--'+childOppList.size());
        
        
        List<Opportunity> updatedOppList = [SELECT Id,
                                            Number_of_Child_Opportunities__c, 
                                            Used_Strategic_Budget__c, 
                                            WIP_Strategic_Budget__c 
                                            FROM Opportunity 
                                            WHERE Id =: parentOppList[0].Id];
        System.debug('--30 test class--'+updatedOppList);
        System.assertEquals(5, updatedOppList.get(0).Number_of_Child_Opportunities__c);
        System.assertEquals(500, updatedOppList[0].Used_Strategic_Budget__c);
        Test.stopTest();
    }
    
    @isTest
    public static void positiveTestForUpdate(){
        
        
        Account acc = TestDataFactory.acctRec();
        
        insert acc;
        
        List<Opportunity> parentOppList = TestDataFactory.getParentOpportunity(acc.Id);
        
        insert parentOppList;
        
        List<Opportunity> childOppList = TestDataFactory.getChildOpportunity(acc.Id, parentOppList[0].id);
        
        insert childOppList;
        
        List<Opportunity> childOppList1 = TestDataFactory.getChildOpportunity(acc.Id, parentOppList[1].id);
        
        insert childOppList1;
        RollupChildOpprotunityHandler.isTriggerFired = false;
        
        List<Opportunity> updateChlidList = [SELECT id, 
                                             Strategic_Opportunity__c
                                             FROM Opportunity 
                                             WHERE Id = : childOppList[0].Id];
        system.debug('before update updateChlidList---- '+updateChlidList[0].Strategic_Opportunity__c);
        
        updateChlidList[0].Strategic_Opportunity__c = parentOppList[1].Id;
        
        Test.startTest();
        update updateChlidList;
        RollupChildOpprotunityHandler.isTriggerFired = true;
        
        System.debug('--Parent[0]--'+parentOppList[0].Id);
        System.debug('--Parent[1]--'+parentOppList[1].Id);
       system.debug(' after update updateChlidList---- '+updateChlidList[0].Strategic_Opportunity__c); 
        
        List<Opportunity> updatedOppList = [SELECT Id,
                                            Number_of_Child_Opportunities__c, 
                                            Used_Strategic_Budget__c, 
                                            WIP_Strategic_Budget__c 
                                            FROM Opportunity 
                                            WHERE Id =: parentOppList[0].Id];
        
        
        System.debug('--68-Testcase-'+updatedOppList);
        
        
        
        System.assertEquals(4, updatedOppList[0].Number_of_Child_Opportunities__c);
        Test.stopTest();
        
    }
    
    @isTest
    public static void positiveTestForDelete(){
        
        
        Account acc = TestDataFactory.acctRec();
        
        insert acc;
        
        List<Opportunity> parentOppList = TestDataFactory.getParentOpportunity(acc.Id);
        
        insert parentOppList;
        
        List<Opportunity> childOppList = TestDataFactory.getChildOpportunity(acc.Id, parentOppList[0].id);
        
        insert childOppList;
        
        
        //RollupChildOpprotunityHandler.isCheck = false;
        
        List<Opportunity> updateChlidList = [SELECT id, 
                                             Strategic_Opportunity__c
                                             FROM Opportunity 
                                             WHERE Id = : childOppList[0].Id];
        
        
        Test.startTest();
        Delete childOppList[0];
        //RollupChildOpprotunityHandler.isCheck = true;
        
        
        
        List<Opportunity> updatedOppList = [SELECT Id,
                                            Number_of_Child_Opportunities__c, 
                                            Used_Strategic_Budget__c, 
                                            WIP_Strategic_Budget__c 
                                            FROM Opportunity 
                                            WHERE Id =: parentOppList[0].Id];
        
        
        System.debug('--68-Testcase-'+updatedOppList);
        
        System.assertEquals(4, updatedOppList[0].Number_of_Child_Opportunities__c);
        Test.stopTest();
        
    }
    
}