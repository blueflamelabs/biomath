/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy     Description
*       1.0      100619         BFL Users        This class is built to Mapping the Child fields which have 'Child Opportunity' 
*                                                RecordType or Update the Parent record which have 'Strategic Opportunity' Record Type.
**********************************************************************************************************************************************************/   

public class RollupChildOpprotunityHandler { 
    public static boolean isTriggerFired = false;

    /**
    * MethodName : childOppotunityRollup
    * Parameters : opportunityList
    * Description : This method is used to roll up amount of Closed and WIP Opportunity records on Strategic Opportunity
    **/
    public static void childOppotunityRollup(List<Opportunity> opportunityList) {
        Id childOppoRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Child Opportunity').getRecordTypeId();
        isTriggerFired = true ;
        Set<Id> setParentId = new Set<Id>();
        List<Opportunity> oppoListToUpdate = new List<Opportunity>();

        // Iterate on newly inserted List to get parent Id
        for(Opportunity opportunity : opportunityList) {
            // Check if opportunity is child Opportunity
            //System.debug('======opportunity.Strategic_Opportunity__c=' + opportunity.Strategic_Opportunity__c); 
            //System.debug('======opportunity.RecordTypeId=' + opportunity.RecordTypeId);    
            if(opportunity.Strategic_Opportunity__c != null 
                && opportunity.RecordTypeId == childOppoRecordTypeId 
            ) {
                setParentId.add(opportunity.Strategic_Opportunity__c);
                
            }
        } // End of for loop
        
        // Get parent Opportunity with their child opportunity
        for(Opportunity parentOpportunity : [ SELECT Id,
                                                     Strategic_Opportunity__c, 
                                                     Number_of_Child_Opportunities__c,
                                                     Used_Strategic_Budget__c,
                                                     Remaining_Strategic_Budget__c,
                                                     WIP_Strategic_Budget__c,
                                                     CloseDate,
                                                     (SELECT Id,
                                                             Strategic_Opportunity__c, 
                                                             StageName, 
                                                             Amount,
                                                             CloseDate 
                                                        FROM Opportunities__r 
                                                       WHERE Strategic_Opportunity__c IN :setParentId
                                                         AND RecordTypeId = :childOppoRecordTypeId
                                                    ORDER BY CreatedDate)
                                                FROM Opportunity 
                                               WHERE Id IN :setParentId
                                    ]) {
            //System.debug('==FOR===parentOpportunity=====' + parentOpportunity);
            parentOpportunity.Number_of_Child_Opportunities__c = parentOpportunity.Opportunities__r.size();
            //parentOpportunity.First_Child_Opportunity_Close_Date__c = parentOpportunity.Opportunities__r[0].CloseDate;
            Decimal closedOpportunityAmount = 0;
            Decimal wipOpportunityAmount = 0;
            List<Date> recentCloseDateList = new List<Date>(); 
            List<Date> firstCloseDateList = new List<Date>(); 
            // Iterate on child record to get amount of closed Opportunity and WIP Opportunity and store it on Strategic Opportunity
            //System.debug('==FOR===child===Opportunity=====' + parentOpportunity.Opportunities__r);
            for(opportunity childOpportunity : parentOpportunity.Opportunities__r) {
                // T-000803 - 131119 - VennScience_BFL_Amruta - Removed the criteria for calculating the Closed Suspended 
                // and Closed Lost child opportunity Amount
                if((childOpportunity.StageName.equalsIgnoreCase('Closed Won'))
                   // && childOpportunity.Amount != null
                ) {
                    //110719 T - 00419 commented childOpportunity closedate condition VennScience_BFL_Monali
                    //if(childOpportunity.CloseDate >= System.today())
                        recentCloseDateList.add(childOpportunity.CloseDate);

                    if(childOpportunity.CloseDate <= System.today())
                        firstCloseDateList.add(childOpportunity.CloseDate);

                    if(childOpportunity.Amount != null)
                        closedOpportunityAmount = closedOpportunityAmount + childOpportunity.Amount;
                    
                } else if(String.isNotEmpty(childOpportunity.StageName) && childOpportunity.Amount != null) {
                    
                    wipOpportunityAmount = wipOpportunityAmount + childOpportunity.Amount;
                    //System.debug('=wipOpportunityCount===FOR====' + wipOpportunityCount);
                }
            }
            
            //System.debug('=======firstCloseDateList====' + firstCloseDateList);
            //System.debug('=======recentCloseDateList====' + recentCloseDateList);

            //recentCloseDateList.sort();
            firstCloseDateList.sort();

            //System.debug('=======listSortedDate=SORT===' + recentCloseDateList);
            parentOpportunity.Used_Strategic_Budget__c = closedOpportunityAmount;
            parentOpportunity.WIP_Strategic_Budget__c = wipOpportunityAmount;
            if(!recentCloseDateList.isEmpty()) {
                //110719 T - 00419 populated the close date value of recently inserted child opportunity VennScience_BFL_Monali
                parentOpportunity.Most_Recent_Child_Opportunity_Close_Date__c = recentCloseDateList[recentCloseDateList.size()-1];
            }
            if(!firstCloseDateList.isEmpty()) {
                parentOpportunity.First_Child_Opportunity_Close_Date__c = firstCloseDateList[firstCloseDateList.size()-1]; 
            }
            oppoListToUpdate.add(parentOpportunity);
        }

        // Update strategic Opportunity
        if(oppoListToUpdate.size() > 0) {
            update oppoListToUpdate;
        }
    }
}