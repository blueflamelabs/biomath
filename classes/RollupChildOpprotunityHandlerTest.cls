@isTest
public class RollupChildOpprotunityHandlerTest {
    
    @isTest
    public static void positiveTestForInsert(){
        
        Account acc = TestDataFactory.acctRec();
        
        insert acc;
        
        List<Opportunity> parentOppList = TestDataFactory.getParentOpportunity(acc.Id);
        insert parentOppList;
        
        List<Opportunity> childOppList = TestDataFactory.getChildOpportunity(acc.Id, parentOppList[0].id);
        
        Test.startTest();
        RollupChildOpprotunityHandler.isTriggerFired = false;
        insert childOppList;
        System.debug('====childOppList===='+childOppList);

        List<Opportunity> updatedOppList = [ SELECT Id,
                                                    Number_of_Child_Opportunities__c, 
                                                    Used_Strategic_Budget__c, 
                                                    WIP_Strategic_Budget__c 
                                               FROM Opportunity 
                                              ];

        System.debug('====updatedOppList===='+updatedOppList);
        System.debug('====updatedOppList[0].Used_Strategic_Budget__c===='+updatedOppList[0].Used_Strategic_Budget__c);
        System.debug('====updatedOppList[0].Number_of_Child_Opportunities__c===='+updatedOppList[0].Number_of_Child_Opportunities__c);
        System.assertEquals(5, updatedOppList[0].Number_of_Child_Opportunities__c);
        System.assertEquals(500, updatedOppList[0].Used_Strategic_Budget__c);
        Test.stopTest();
    }
    
    @isTest
    public static void positiveTestForDelete(){
        
        
        Account acc = TestDataFactory.acctRec();
        
        insert acc;
        
        List<Opportunity> parentOppList = TestDataFactory.getParentOpportunity(acc.Id);
        
        insert parentOppList;
        
        List<Opportunity> childOppList = TestDataFactory.getChildOpportunity(acc.Id, parentOppList[0].id);
        RollupChildOpprotunityHandler.isTriggerFired = false;
        insert childOppList;
        
        
        //RollupChildOpprotunityHandler.isCheck = false;
        
        List<Opportunity> updateChlidList = [SELECT id, 
                                             Strategic_Opportunity__c
                                             FROM Opportunity 
                                             WHERE Id = : childOppList[0].Id];
        
        
        Test.startTest();
        RollupChildOpprotunityHandler.isTriggerFired = false;
        Delete childOppList[0];
        //RollupChildOpprotunityHandler.isCheck = true;
        
        
        
        List<Opportunity> updatedOppList = [SELECT Id,
                                            Number_of_Child_Opportunities__c, 
                                            Used_Strategic_Budget__c, 
                                            WIP_Strategic_Budget__c 
                                            FROM Opportunity 
                                            WHERE Id =: parentOppList[0].Id];
        
        
        System.debug('--68-Testcase-'+updatedOppList);
        
        System.assertEquals(4, updatedOppList[0].Number_of_Child_Opportunities__c);
        Test.stopTest();
        
    }
    
}