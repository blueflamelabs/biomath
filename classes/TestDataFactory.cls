public class TestDataFactory {
    
    public static Account acctRec(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        
        return acc;
    }
    
    public static List<Opportunity> getParentOpportunity(Id AccountId){
        ID rectypeid = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Strategic Opportunity').getRecordTypeId();
        
        List<Opportunity> parentOppList = new List<Opportunity>();
        
        for(Integer i=0; i<3; i++){
            Opportunity opp = new Opportunity();
            opp.AccountId = AccountId;
            opp.Name = 'Test Opportunity';
            opp.StageName = 'Qualification';
            opp.CloseDate = System.today();
            opp.RecordTypeId = rectypeid;
            parentOppList.add(opp);
        }
        
        
        return parentOppList;
    }
    
    public static List<Opportunity> getChildOpportunity(Id AccountId, Id ParentOppId){
        ID rectypeid = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Child Opportunity').getRecordTypeId();
        List<Opportunity> oppList = new List<Opportunity>();
        for(Integer i=0; i< 5; i++){
            Opportunity opp = new Opportunity();
            opp.AccountId = AccountId;
            opp.Name = 'Test Child Opportunity'+i;
            opp.StageName = 'Closed Won';
            opp.CloseDate = System.today();
            opp.Amount = 100;
            opp.Strategic_Opportunity__c = ParentOppId;
            opp.RecordTypeId = rectypeid;
            oppList.add(opp);
        }
        
        return oppList;
    }

}