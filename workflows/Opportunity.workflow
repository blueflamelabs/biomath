<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Total_Score_Value</fullName>
        <field>Total_Score_Value__c</field>
        <formula>Total_Score__c</formula>
        <name>Update Total Score Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Total Score Value</fullName>
        <actions>
            <name>Update_Total_Score_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISNEW() ,

 ISCHANGED( StageName ),
 ISCHANGED(  Champion__c  ),
 ISCHANGED(  Competition_Neutralized__c  ),
 ISCHANGED(  Confidence__c  ),
 ISCHANGED(  Critical_Business_Issue_Defined__c  ),
 ISCHANGED(  High_Level_Support__c  ),
 ISCHANGED(  Relations__c  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
