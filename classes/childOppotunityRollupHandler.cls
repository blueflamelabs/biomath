public class childOppotunityRollupHandler {
    
    public static void beforeInsertRollupTrigger(List<Opportunity> opplist,Map<Id,Opportunity> newMap){
        handleBussinessRules(opplist,null);
        
    }
    
    public static void beforeUpdateRollupTrigger(List<Opportunity> opplist, Map<Id, Opportunity> newMap,List<Opportunity> oldOppList, Map<Id, Opportunity> oldMap){
        
        handleBussinessRules(opplist,oldMap );
        
    }
    
    public static void beforeDeleteRollupTrigger(List<Opportunity> oldOppList, Map<Id, Opportunity> oldMap){
        handleBussinessRules(oldOppList,oldMap );
        
    }
    
    private static void handleBussinessRules(List<Opportunity> oppList, Map<Id,Opportunity> oppMap ){
        if(oppList == null || oppList.isempty() ){
            return;
        }
        
        Map<Id,Integer> newOppMap = new Map<Id,Integer>();    
        Map<Id,Decimal> rollup_xmap = new Map<Id,Decimal>();
        Map<Id,Decimal> rollup_ymap = new Map<Id,Decimal>();
        
            for(Opportunity opp : oppList){
                if(opp.Strategic_Opportunity__c != null){
                    if(oppMap != null){
                        if(opp.Strategic_Opportunity__c != oppMap.get(opp.Id).Strategic_Opportunity__c){
                            newOppMap.put(oppMap.get(opp.Id).Strategic_Opportunity__c, -oppMap.size());
                            System.debug('uppp--->'+oppMap.get(opp.Id).Strategic_Opportunity__c);
                            System.debug('updatecon---->'+newOppMap);
                            if(opp.StageName == 'Closed Won' || opp.StageName == 'Closed Lost' || opp.StageName =='Closed Suspended'){
                                rollup_xmap.put(oppMap.get(opp.Id).Strategic_Opportunity__c, - oppMap.get(opp.Id).Amount);
                            }else{
                                rollup_ymap.put(oppMap.get(opp.Id).Strategic_Opportunity__c, - oppMap.get(opp.Id).Amount);
                            }
                        }else{
                            System.debug('updatecon---->'+oppMap);
                            newOppMap.put(oppMap.get(opp.Id).Strategic_Opportunity__c, - oppMap.size());
                            if(opp.StageName == 'Closed Won' || opp.StageName == 'Closed Lost' || opp.StageName =='Closed Suspended'){
                                rollup_xmap.put(oppMap.get(opp.Id).Strategic_Opportunity__c, - oppMap.get(opp.Id).Amount);
                            }else{
                                rollup_ymap.put(oppMap.get(opp.Id).Strategic_Opportunity__c, - oppMap.get(opp.Id).Amount);
                            }
                        }
                    }
                    
                    if(!Trigger.isDelete){
                        Integer count1 = 0;
                        Decimal x = opp.Amount;
                        Decimal y = opp.Amount;
                        if(newOppMap.get(opp.Strategic_Opportunity__c) != null ){
                            newOppMap.put(opp.Strategic_Opportunity__c, newOppMap.get(opp.Strategic_Opportunity__c) + 1);
                            System.debug('newOppMap---->'+newOppMap);
                            if(opp.StageName == 'Closed Won' || opp.StageName == 'Closed Lost' || opp.StageName =='Closed Suspended'){
                                rollup_xmap.put(opp.Strategic_Opportunity__c, rollup_xmap.get(opp.Strategic_Opportunity__c) + x);
                                
                            }else{
                                rollup_ymap.put(opp.Strategic_Opportunity__c, rollup_ymap.get(opp.Strategic_Opportunity__c) + y);
                            }
                            
                        }else{
                            System.debug('--70--');
                            count1++;
                            if(opp.StageName == 'Closed Won' || opp.StageName == 'Closed Lost' || opp.StageName =='Closed Suspended'){
                                System.debug('--73--'+rollup_xmap);
                                if(x != null){
                                   rollup_xmap.put(opp.Strategic_Opportunity__c, x); 
                                }
                                
                                
                            }else{
                                if(y != null){
                                    rollup_ymap.put(opp.Strategic_Opportunity__c, y);
                                }
                            }
                            newOppMap.put(opp.Strategic_Opportunity__c, count1);
                        }
                    }
                }
            }
        
        
        
        updateCount(newOppMap, rollup_xmap, rollup_ymap);
    }
    
    private static void updateCount(Map<Id,Integer> newOppMap, Map<Id,Decimal> rollup_xmap,Map<Id,Decimal> rollup_ymap){  
        System.debug('==103=='+rollup_ymap);
        if(newOppMap == null ||  newOppMap.isempty() || rollup_xmap == null || rollup_xmap.isempty()){
            System.debug('inside if');
            return;
        }
        
        
        List<Opportunity> oppList = [Select Id, Used_Strategic_Budget__c, WIP_Strategic_Budget__c, Strategic_Opportunity__c, Number_of_Child_Opportunities__c, Most_Recent_Child_Opportunity_Close_Date__c
                                     From Opportunity where Id IN : newOppMap.keyset()];
        
        for(Opportunity op : oppList){
            if(op.Number_of_Child_Opportunities__c != null){
                op.Number_of_Child_Opportunities__c = op.Number_of_Child_Opportunities__c + newOppMap.get(op.Id);
            }else{
                op.Number_of_Child_Opportunities__c =  newOppMap.get(op.Id);
            }
            if(op.Used_Strategic_Budget__c != null &&  rollup_xmap.containsKey(op.Id)){
                op.Used_Strategic_Budget__c = op.Used_Strategic_Budget__c + rollup_xmap.get(op.Id);
            }else{
                op.Used_Strategic_Budget__c =  rollup_xmap.get(op.Id);
            }
            if(rollup_ymap != null && !rollup_ymap.isempty() && op.WIP_Strategic_Budget__c != null){
                op.WIP_Strategic_Budget__c = op.WIP_Strategic_Budget__c + rollup_ymap.get(op.Id);
            }else{
                op.WIP_Strategic_Budget__c =  rollup_ymap.get(op.Id);
            }
        }
        Update oppList;
        
        
    }
    
}