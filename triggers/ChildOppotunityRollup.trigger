trigger ChildOppotunityRollup on Opportunity (after Insert, after update, after delete) {
    
    if(RollupChildOpprotunityHandler.isTriggerFired == false){
        if(Trigger.IsAfter && Trigger.IsDelete ) { 
            RollupChildOpprotunityHandler.childOppotunityRollup(Trigger.old);
        } else if(Trigger.IsAfter && (Trigger.IsInsert  || Trigger.IsUpdate)) {
            RollupChildOpprotunityHandler.childOppotunityRollup(Trigger.new);
        }        
    }
    
    
    /*if(Trigger.isafter){
        
        if(Trigger.isInsert) {
            //100619-T-00216-Call the method on Insert event from handler.
            system.debug('---- Insert trigger start----');
            RollupChildOpprotunityHandler.rollUpOfChildOpp(Trigger.new, null);
             system.debug('---- Insert trigger end----');
        }
        //100619-T-00216-Call the method on Update event from handler
        
        if(Trigger.isUpdate) {
            if(RollupChildOpprotunityHandler.isCheck == true){
                
                 system.debug('---- update trigger start----');
                
                system.debug('----Trigger.oldMap----'+Trigger.oldMap);
                system.debug('----Trigger.NewMap----'+Trigger.new);
                 RollupChildOpprotunityHandler.rollUpOfChildOpp(Trigger.new,Trigger.oldMap);
                system.debug('---- update trigger end----');
            }
           
        }
        //100619-T-00216-Call the method on Delete event from handler.
        
        if(Trigger.isDelete) {
           RollupChildOpprotunityHandler.rollUpOfChildOpp(Trigger.old,Trigger.oldMap);
        }
        
    }*/

}