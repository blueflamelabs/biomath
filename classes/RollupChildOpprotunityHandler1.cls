/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy     Description
*       1.0      100619      BFL Users        This class is built to Mapping the Child fields which have 'Child Opportunity' 
*                                             RecordType or Update the Parent record which have 'Strategic Opportunity' Record Type.
**********************************************************************************************************************************************************/   

public class RollupChildOpprotunityHandler1 {
    
    //100619-T-00216-to prevent the recursion.
    public static boolean  isTriggerFired = false;
    
    //100619-T-00216-to rollup the amount, count the child Opportunity and populate on the parent opportunity.
    public static void rollUpOfChildOpp(List<Opportunity> newOpportunityList) { 
        Id childOppoRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Child Opportunity').getRecordTypeId();
        isTriggerFired = true;
        // This will store parent and its related child 
        Map<Id, List<Opportunity>> parentChildOppoMap = new Map<Id, List<Opportunity>>();
        List<Opportunity> oppoListToUpdate = new List<Opportunity>();
        Set<Id> parentId = new Set<Id>();
        Map<Id,Opportunity> mapOfParentOppToUpdate = new Map<Id,Opportunity>();
        //Map<Id,Opportunity> mapOfChildOpp = new Map<Id,Opportunity>();
        
        Decimal wipCount, closedCount;
        System.debug('=====Called=====');
        //List<Opportunity> oppList = trigger.isDelete ? oldOpportunityMap.values(): newOpportunityList;
        if(Trigger.IsAfter && (Trigger.IsInsert  || Trigger.isUpdate) ) { 
            for(Opportunity opportunity : newOpportunityList) {
                // Check if opportunity is child Opportunity
                if(opportunity.Strategic_Opportunity__c != null 
                    && opportunity.RecordTypeId == childOppoRecordTypeId 
                ) {
                    parentId.add(opportunity.Strategic_Opportunity__c);
                    
                }
            } // End of for loop
            System.debug('======parentId=========' + parentId);
            for(Opportunity parentOpportunity : [SELECT Id,
                                          Strategic_Opportunity__c, 
                                          Number_of_Child_Opportunities__c,
                                          Used_Strategic_Budget__c,
                                          Remaining_Strategic_Budget__c,
                                          WIP_Strategic_Budget__c,
                                          (Select Id,Strategic_Opportunity__c, StageName, Amount from Opportunities__r WHERE Strategic_Opportunity__c IN :parentId)
                                     FROM Opportunity 
                                     WHERE Id IN :parentId
                                    ]) {
               System.debug('===parentOpportunity ========' + parentOpportunity );                      
               parentOpportunity.Number_of_Child_Opportunities__c = parentOpportunity.Opportunities__r.size();
               System.debug('==Child count=======' + parentOpportunity.Number_of_Child_Opportunities__c ); 
               wipCount = 0 ;
               closedCount = 0 ;
               for(opportunity childOpportunity : parentOpportunity.Opportunities__r) {
                   if(childOpportunity.StageName == 'Closed Won' 
                        || childOpportunity.StageName == 'Closed Lost' 
                        || childOpportunity.StageName =='Closed Suspended'
                    ) {
                        // update Used_Strategic_Budget__c 
                        System.debug('===closedCount =========' + closedCount ); 
                        if(childOpportunity.Amount != null ){
                            closedCount = closedCount + childOpportunity.Amount;
                            
                        }
                        System.debug('=222==closedCount =========' + closedCount ); 
                    } else {
                        // WIP_Strategic_Budget__c                        
                        System.debug('===wipCount =========' + wipCount ); 
                        if(childOpportunity.Amount != null ){
                            wipCount = wipCount + childOpportunity.Amount;
                            
                        }
                        System.debug('=222==wipCount =========' + wipCount ); 
                    }
               }
               System.debug('=END==closedCount =========' + closedCount );
               System.debug('=END==wipCount =========' + wipCount ); 
               parentOpportunity.Used_Strategic_Budget__c = closedCount ;
               parentOpportunity.WIP_Strategic_Budget__c = wipCount ; 
               oppoListToUpdate.add(parentOpportunity);                    
            }
           System.debug('===oppoListToUpdate=========' + oppoListToUpdate ); 
           update oppoListToUpdate;
           }    
                                 
        }
    }